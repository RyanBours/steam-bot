﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Steam_Bot__UI_ {
    public partial class Login : Form {

        public string user = string.Empty, pass = string.Empty;

        public Login() {
            InitializeComponent();

            passwordBox.KeyDown += (sender, args) => {
                if (args.KeyCode == Keys.Enter) button1.PerformClick();
            };
        }

        private void button1_Click(object sender, EventArgs e) {
            user = usernameBox.Text.Trim();
            pass = passwordBox.Text.Trim();
        }

        // TODO: onFormclosing trough the close button not just when is closes
        protected override void OnFormClosing(FormClosingEventArgs e) {
            if (e.CloseReason == CloseReason.UserClosing) Application.Exit();
        }

    }
}
