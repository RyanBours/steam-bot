﻿using SteamKit2;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;

namespace Steam_Bot__UI_ {
    public partial class MainUI : Form {

        // http://www.dotnet-shrikant.com/frmPopUp.aspx

        static string user, pass;
        static bool isRunning;

        static SteamClient steamClient;
        static CallbackManager manager;
        static SteamUser steamUser;
        static SteamFriends steamFriends;

        static string authCode, twoFactorAuth;

        List<string> SendCommand = new List<string>(); // [0]: Command name <string>.

        public MainUI() {
            InitializeComponent();

            Login login = new Login();
            DialogResult loginResult = login.ShowDialog();
            if (loginResult == DialogResult.OK) {
                // Console.WriteLine("input: {0}, {1}", login.user, login.pass); // DEBUG
                user = login.user;
                pass = login.pass;

                new Thread(() => {
                    Thread.CurrentThread.IsBackground = true;
                    SteamLogIn();
                }).Start();
            }

        }

        private void MainUI_Load(object sender, EventArgs e) {
            loadCommands();
            buttonCommandReload.Enabled = true;
            /* Command Button loading and ui Resetting*/
            buttonCommandReload.Click += (eventSender, eventArgs) => {
                ResetUI();
                Console.WriteLine("Loading Commands.");
                loadCommands();
            };

            /* Command Execution */
            CommandSendButton.Click += delegate {
                foreach (string s in SendCommand) Console.WriteLine(s);
                switch (SendCommand[0]) {
                    case "send":
                        steamFriends.SendChatMessage(new SteamID(SendCommand[1]), EChatEntryType.ChatMsg, SendCommand[2]);
                        break;

                    default:
                        Console.WriteLine("whoops");
                        break;
                }
            };
        }

        protected override void OnFormClosing(FormClosingEventArgs e) {
            isRunning = false;
            Application.Exit();
            Environment.Exit(0);
        }

        private void ResetUI() {
            CommandPanel.Controls.Clear();
            optionLayoutPanel.Controls.Clear();
            CommandSendButton.Enabled = false;
        }

        private void loadCommands() {
            #region Send Command OLD
            /* Send Command */
            Button CommandSend = new Button() { Text = "send", Dock = DockStyle.Top, Location = new Point(CommandPanel.Location.X, CommandPanel.Location.Y) };
            CommandSend.Click += delegate {
                SendCommand.Clear(); // clear the SendCommand list to prevent problems
                SendCommand.Add("");// this fixes
                SendCommand.Add("");// the out of
                SendCommand.Add("");// range exception
                SendCommand.Add("");
                SendCommand[0] = "send"; // FIXME: out of range exception

                optionLayoutPanel.Controls.Clear(); // Remove all the controls in the 'Options' Groupbox.
                optionLayoutPanel.ColumnCount = 2; // set collumn amount to the neccessary amount of option

                /* Controls */

                Label command = new Label() { Dock = DockStyle.Top };

                /* friend selection */
                Panel pl1 = new Panel();
                pl1.Dock = DockStyle.Fill;
                pl1.BackColor = SystemColors.ControlLight;
                pl1.AutoScroll = true;
                for (int i = 0; i < steamFriends.GetFriendCount(); i++) {
                    SteamID steamIdFriend = steamFriends.GetFriendByIndex(i);
                    Button f = new Button() { Text = steamFriends.GetFriendPersonaName(steamIdFriend), Dock = DockStyle.Top, Location = new Point(pl1.Location.X, pl1.Location.Y) };
                    f.Click += delegate { SendCommand[1] = steamIdFriend.Render().ToString(); Console.WriteLine(SendCommand[1]); CommandSendButton.Enabled = (SendCommand[1] != "" && SendCommand[2] != "") ? true : false; command.Text = String.Format("To: {0}, Message: {1}", steamFriends.GetFriendPersonaName(new SteamID(SendCommand[1])), SendCommand[2]); };
                    pl1.Controls.Add(f);
                }
                optionLayoutPanel.Controls.Add(pl1); // add controls to 'options'

                /* Message */
                Panel pl2 = new Panel();
                pl2.Dock = DockStyle.Fill;
                pl2.BackColor = SystemColors.ControlLight;

                Label lb = new Label() { Text = "Enter your desired message", Dock = DockStyle.Top };
                TextBox tb = new TextBox() { Dock = DockStyle.Top };
                Button ub = new Button();
                ub.Text = "Update Message";
                ub.Dock = DockStyle.Top;
                ub.Click += delegate { SendCommand[2] = tb.Text; CommandSendButton.Enabled = (SendCommand[1] != "" && SendCommand[2] != "") ? true : false; command.Text = String.Format("To: {0}, Message: {1}", steamFriends.GetFriendPersonaName(new SteamID(SendCommand[1])), SendCommand[2]); };

                pl2.Controls.Add(command);
                pl2.Controls.Add(ub);
                pl2.Controls.Add(tb);
                pl2.Controls.Add(lb);

                optionLayoutPanel.Controls.Add(pl2);

            };
            CommandPanel.Controls.Add(CommandSend);
            #endregion
        }

        static void SteamLogIn() {
            steamClient = new SteamClient();
            manager = new CallbackManager(steamClient);

            steamUser = steamClient.GetHandler<SteamUser>();
            steamFriends = steamClient.GetHandler<SteamFriends>();


            manager.Subscribe<SteamClient.ConnectedCallback>(OnConnected);
            manager.Subscribe<SteamClient.DisconnectedCallback>(OnDisconnected);

            manager.Subscribe<SteamUser.LoggedOnCallback>(OnLoggedOn);
            manager.Subscribe<SteamUser.LoggedOffCallback>(OnLoggedOff);

            manager.Subscribe<SteamUser.UpdateMachineAuthCallback>(OnMachineAuth);


            manager.Subscribe<SteamUser.AccountInfoCallback>(OnAccountInfo);
            manager.Subscribe<SteamFriends.FriendsListCallback>(OnFriendsList);
            manager.Subscribe<SteamFriends.PersonaStateCallback>(OnPersonaState);
            manager.Subscribe<SteamFriends.FriendAddedCallback>(OnFriendAdded);

            steamClient.Connect();
            isRunning = true;
            while (isRunning) {
                manager.RunWaitCallbacks(TimeSpan.FromSeconds(1));
            }
        }

        static void OnMachineAuth(SteamUser.UpdateMachineAuthCallback callback) {
            Console.WriteLine("Updating sentry file...");

            int fileSize;
            byte[] sentryHash;
            using (var fs = File.Open("sentry.bin", FileMode.OpenOrCreate, FileAccess.ReadWrite)) {
                fs.Seek(callback.Offset, SeekOrigin.Begin);
                fs.Write(callback.Data, 0, callback.BytesToWrite);
                fileSize = (int)fs.Length;

                fs.Seek(0, SeekOrigin.Begin);
                using (var sha = SHA1.Create()) {
                    sentryHash = sha.ComputeHash(fs);
                }
            }

            steamUser.SendMachineAuthResponse(new SteamUser.MachineAuthDetails {
                JobID = callback.JobID,
                FileName = callback.FileName,
                BytesWritten = callback.BytesToWrite,
                FileSize = callback.Data.Length,
                Offset = callback.Offset,
                Result = EResult.OK,
                LastError = 0,
                OneTimePassword = callback.OneTimePassword,
                SentryFileHash = sentryHash,
            });

            Console.WriteLine("Done");
        }

        static void OnConnected(SteamClient.ConnectedCallback callback) {
            if (callback.Result != EResult.OK) {
                Console.WriteLine("Unable to Connect to Steam: {0}", callback.Result);
                isRunning = false;
                return;
            }
            Console.WriteLine("Connected to Steam. \nLogging in {0}...\n", callback.Result);

            byte[] sentryHash = null;

            if (File.Exists("sentry.bin")) {
                byte[] sentryFile = File.ReadAllBytes("sentry.bin");
                sentryHash = CryptoHelper.SHAHash(sentryFile);
            }

            try {
                steamUser.LogOn(new SteamUser.LogOnDetails {
                    Username = user,
                    Password = pass,

                    AuthCode = authCode,
                    TwoFactorCode = twoFactorAuth,

                    SentryFileHash = sentryHash,
                });
            } catch {
                Application.Exit();
                Environment.Exit(0);
            }

            isRunning = true;
        }
        static void OnDisconnected(SteamClient.DisconnectedCallback callback) {
            Console.WriteLine("\n{0} disconnected from steam reconnecting in 5...\n", user);
            Thread.Sleep(TimeSpan.FromSeconds(5));
            steamClient.Connect();
        }

        static void OnLoggedOn(SteamUser.LoggedOnCallback callback) {
            /* 2FA | Guard check */
            bool isSteamGuard = callback.Result == EResult.AccountLogonDenied;
            bool is2FA = callback.Result == EResult.AccountLoginDeniedNeedTwoFactor;

            if (isSteamGuard | is2FA) {
                Console.WriteLine("This account is SteamGuard protected!");
                if (is2FA | isSteamGuard) {
                    Auth auth = new Auth((is2FA) ? Auth.AuthType.TWOFACTOR : Auth.AuthType.CODE);
                    DialogResult authResult = auth.ShowDialog();
                    if (authResult == DialogResult.OK) {
                        if (is2FA) twoFactorAuth = auth.authcode;
                        else authCode = auth.authcode;
                    }
                    return;
                }
            }

            if (callback.Result == EResult.InvalidPassword) {
                MessageBox.Show("Your login information is invalid. Please restart the app.");
                Application.Exit();
                Environment.Exit(0);

            }

            if (callback.Result != EResult.OK) {
                Console.WriteLine("Unable to log in Steam: {0}\n", callback.Result);
                isRunning = false;
                return;
            }
            Console.WriteLine("{0} succesfully logged in!", user);
        }
        static void OnLoggedOff(SteamUser.LoggedOffCallback callback) {
            Console.WriteLine("Logged off of Steam: {0}", callback.Result);
        }

        static void OnAccountInfo(SteamUser.AccountInfoCallback callback) {
            steamFriends.SetPersonaState(EPersonaState.Offline);
        }

        static void OnFriendsList(SteamFriends.FriendsListCallback callback) {
            // at this point, the client has received it's friends list

            int friendCount = steamFriends.GetFriendCount();

            Console.WriteLine("We have {0} friends", friendCount);
            //int onlineCount = 0;

            for (int i = 0; i < friendCount; i++) {
                SteamID steamIdFriend = steamFriends.GetFriendByIndex(i);
                Console.WriteLine("Friend: {0}", steamIdFriend.Render(true));
            }

            foreach (var friend in callback.FriendList) {

                /* add on invite */
                if (friend.Relationship == EFriendRelationship.RequestRecipient) {
                    steamFriends.AddFriend(friend.SteamID);
                }
            }

        }

        static void OnFriendAdded(SteamFriends.FriendAddedCallback callback) {
            Console.WriteLine("{0} is now a friend", callback.PersonaName);
        }

        static void OnPersonaState(SteamFriends.PersonaStateCallback callback) {
            Console.WriteLine("State change: {0}", callback.Name);
        }

    }
}
