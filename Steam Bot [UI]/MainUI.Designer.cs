﻿namespace Steam_Bot__UI_ {
    partial class MainUI {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonCommandReload = new System.Windows.Forms.Button();
            this.CommandPanel = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.optionLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.CommandSendButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonCommandReload);
            this.groupBox1.Controls.Add(this.CommandPanel);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(192, 441);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Commands";
            // 
            // buttonCommandReload
            // 
            this.buttonCommandReload.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonCommandReload.Enabled = false;
            this.buttonCommandReload.Location = new System.Drawing.Point(3, 391);
            this.buttonCommandReload.Name = "buttonCommandReload";
            this.buttonCommandReload.Size = new System.Drawing.Size(186, 47);
            this.buttonCommandReload.TabIndex = 1;
            this.buttonCommandReload.Text = "Reload Commands";
            this.buttonCommandReload.UseVisualStyleBackColor = true;
            // 
            // CommandPanel
            // 
            this.CommandPanel.AutoScroll = true;
            this.CommandPanel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.CommandPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CommandPanel.Location = new System.Drawing.Point(3, 16);
            this.CommandPanel.Name = "CommandPanel";
            this.CommandPanel.Size = new System.Drawing.Size(186, 422);
            this.CommandPanel.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.optionLayoutPanel);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(192, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(687, 441);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Options";
            // 
            // optionLayoutPanel
            // 
            this.optionLayoutPanel.BackColor = System.Drawing.SystemColors.Control;
            this.optionLayoutPanel.ColumnCount = 2;
            this.optionLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.optionLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.optionLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optionLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.optionLayoutPanel.Name = "optionLayoutPanel";
            this.optionLayoutPanel.RowCount = 1;
            this.optionLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.optionLayoutPanel.Size = new System.Drawing.Size(681, 422);
            this.optionLayoutPanel.TabIndex = 0;
            // 
            // CommandSendButton
            // 
            this.CommandSendButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.CommandSendButton.Enabled = false;
            this.CommandSendButton.Location = new System.Drawing.Point(192, 393);
            this.CommandSendButton.Name = "CommandSendButton";
            this.CommandSendButton.Size = new System.Drawing.Size(687, 48);
            this.CommandSendButton.TabIndex = 2;
            this.CommandSendButton.Text = "Send";
            this.CommandSendButton.UseVisualStyleBackColor = true;
            // 
            // MainUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 441);
            this.Controls.Add(this.CommandSendButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "MainUI";
            this.Text = "Steam Bot";
            this.Load += new System.EventHandler(this.MainUI_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button CommandSendButton;
        private System.Windows.Forms.Panel CommandPanel;
        private System.Windows.Forms.Button buttonCommandReload;
        private System.Windows.Forms.TableLayoutPanel optionLayoutPanel;
    }
}

