﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Steam_Bot__UI_ {
    public partial class Auth : Form {

        public enum AuthType {
            CODE,
            TWOFACTOR
        }

        public string authcode;

        public Auth(AuthType authtype) {
            InitializeComponent();

            textBox1.KeyDown += (sender, args) => {
                if (args.KeyCode == Keys.Enter) button1.PerformClick();
            };
        }

        protected override void OnFormClosing(FormClosingEventArgs e) {
            if (e.CloseReason == CloseReason.UserClosing) Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e) {
            authcode = textBox1.Text.Trim();
        }

    }
}
