﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SteamKit2;

namespace Steam_Bot {

    class Commands {

        /* info
         * 
         * !send johndoe Hello World!
         * 1. !send
         * 2. johndoe
         * 3. Hello World!
         * 
         * ArgAmount = 3;
         * 
         */

        public class CommandError : ICommandError {
            // this command is used when the commandstring isn't right.

            public void Action(SteamFriends steamFriends, SteamFriends.FriendMsgCallback callback, string args) {
                steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "You f'ed up!");

                string response = Util.CommandName(args);
                steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, (CommandManager.IsCommandOrAlias(response)) ? CommandManager.GetCommand(response).UsageSyntax : String.Format("'{0}' this is not an available command!"));
                       
                steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "Use the '!help' command the see all the available commands!");
            }
        }

        public class CommandHelp : ICommand {
            public string Description { get => "Shows all the available commands."; }

            public int ArgAmount { get => 1; }
            public string UsageSyntax { get => "'!help' or '!help [command]' for more info about a command!"; }

            public void Action(SteamFriends steamFriends, SteamFriends.FriendMsgCallback callback, string[] args) {
                // loop through command list check if command is not "-1" else send back the command
                foreach (string s in CommandManager.GetCommands()) {
                    steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, String.Format("{0} - {1}", s, CommandManager.GetCommand(s).Description));
                }
                
            }
        }

        public class CommandAlias : ICommand {
            public string Description { get => "No Description available"; }

            public int ArgAmount { get => 1; }
            public string UsageSyntax { get => "not yet set"; }

            public void Action(SteamFriends steamFriends, SteamFriends.FriendMsgCallback callback, string[] args) {
                throw new NotImplementedException();
                // loop throug all the aliasses
            }
        }

        public class CommandTrump : ICommand {
            public string Description { get => "No Description available"; }
            public int ArgAmount { get => 3; }
            public string UsageSyntax { get => "!trump [target] [amount]"; }

            public void Action(SteamFriends steamFriends, SteamFriends.FriendMsgCallback callback, string[] args) {
                Console.WriteLine("Trump die doo!");
                for (int i = 0; i < steamFriends.GetFriendCount(); i++) {
                    SteamID friend = steamFriends.GetFriendByIndex(i);
                    if (steamFriends.GetFriendPersonaName(friend).ToLower().Contains(args[1].ToLower())) {
                        try {
                            int times = Int32.Parse(args[2].ToString());
                            for (int t = 0; t < times; t++) steamFriends.SendChatMessage(friend, EChatEntryType.ChatMsg, ":trump:");
                        } catch {
                            steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, UsageSyntax);
                        }
                    }
                }
            }
        }

        public class CommandBernie : ICommand {
            public string Description { get => "No Description available"; }
            public int ArgAmount { get => 3; }
            public string UsageSyntax { get => "!bernie [target] [amount]"; }

            public void Action(SteamFriends steamFriends, SteamFriends.FriendMsgCallback callback, string[] args) {
                Console.WriteLine("Bernie Palooza!");
                for (int i = 0; i < steamFriends.GetFriendCount(); i++) {
                    SteamID friend = steamFriends.GetFriendByIndex(i);
                    if (steamFriends.GetFriendPersonaName(friend).ToLower().Contains(args[1].ToLower())) {
                        try {
                            int times = Int32.Parse(args[2].ToString());
                            for (int t = 0; t < times; t++) steamFriends.SendChatMessage(friend, EChatEntryType.ChatMsg, ":bernie:");
                        } catch {
                            steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, UsageSyntax);
                        }
                    }
                }
            }
        }

    }
}
