﻿using System;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Collections;
using SteamKit2;

namespace Steam_Bot {
    class Program {

        static string user, pass;
        static bool isRunning;

        static SteamClient steamClient;
        static CallbackManager manager;
        static SteamUser steamUser;
        static SteamFriends steamFriends;

        static string authCode, twoFactorAuth;

        static void Main(string[] args) {

            Console.Title = "DAEMON AUTO CHAT RESPONDER";
            Console.WriteLine("CTRL+C to quit the bot");

            Console.Write("Username: ");
            user = Console.ReadLine();

            Console.Write("Password: ");
            Console.ForegroundColor = ConsoleColor.Black;
            pass = Console.ReadLine();

            Console.Clear();
            Console.ResetColor();

            Console.WriteLine("CTRL+C to quit the bot");
            Console.WriteLine("Send '!help' for the command list");

            CommandManager.Init();
            SteamLogIn();
        }

        static void SteamLogIn() {
            steamClient = new SteamClient();
            manager = new CallbackManager(steamClient);

            steamUser = steamClient.GetHandler<SteamUser>();
            steamFriends = steamClient.GetHandler<SteamFriends>();


            manager.Subscribe<SteamClient.ConnectedCallback>(OnConnected);
            manager.Subscribe<SteamClient.DisconnectedCallback>(OnDisconnected);

            manager.Subscribe<SteamUser.LoggedOnCallback>(OnLoggedOn);
            manager.Subscribe<SteamUser.LoggedOffCallback>(OnLoggedOff);

            manager.Subscribe<SteamUser.UpdateMachineAuthCallback>(OnMachineAuth);


            manager.Subscribe<SteamUser.AccountInfoCallback>(OnAccountInfo);
            manager.Subscribe<SteamFriends.FriendsListCallback>(OnFriendsList);
            manager.Subscribe<SteamFriends.PersonaStateCallback>(OnPersonaState);
            manager.Subscribe<SteamFriends.FriendAddedCallback>(OnFriendAdded);

            manager.Subscribe<SteamFriends.FriendMsgCallback>(OnChatMessage);

            steamClient.Connect();
            isRunning = true;
            while (isRunning) {
                manager.RunWaitCallbacks(TimeSpan.FromSeconds(1));
            }
        }

        static void OnMachineAuth(SteamUser.UpdateMachineAuthCallback callback) {
            Console.WriteLine("Updating sentry file...");

            int fileSize;
            byte[] sentryHash;
            using (var fs = File.Open("sentry.bin", FileMode.OpenOrCreate, FileAccess.ReadWrite)) {
                fs.Seek(callback.Offset, SeekOrigin.Begin);
                fs.Write(callback.Data, 0, callback.BytesToWrite);
                fileSize = (int)fs.Length;

                fs.Seek(0, SeekOrigin.Begin);
                using (var sha = SHA1.Create()) {
                    sentryHash = sha.ComputeHash(fs);
                }
            }

            steamUser.SendMachineAuthResponse(new SteamUser.MachineAuthDetails {
                JobID = callback.JobID,
                FileName = callback.FileName,
                BytesWritten = callback.BytesToWrite,
                FileSize = callback.Data.Length,
                Offset = callback.Offset,
                Result = EResult.OK,
                LastError = 0,
                OneTimePassword = callback.OneTimePassword,
                SentryFileHash = sentryHash,
            });

            Console.WriteLine("Done");
        }

        static void OnConnected(SteamClient.ConnectedCallback callback) {
            if (callback.Result != EResult.OK) {
                Console.WriteLine("Unable to Connect to Steam: {0}", callback.Result);
                isRunning = false;
                return;
            }
            Console.WriteLine("Connected to Steam. \nLogging in {0}...\n", callback.Result);

            byte[] sentryHash = null;

            if (File.Exists("sentry.bin")) {
                byte[] sentryFile = File.ReadAllBytes("sentry.bin");
                sentryHash = CryptoHelper.SHAHash(sentryFile);
            }

            steamUser.LogOn(new SteamUser.LogOnDetails {
                Username = user,
                Password = pass,

                AuthCode = authCode,
                TwoFactorCode = twoFactorAuth,

                SentryFileHash = sentryHash,
            });
        }
        static void OnDisconnected(SteamClient.DisconnectedCallback callback) {
            Console.WriteLine("\n{0} disconnected from steam reconnecting in 5...\n", user);
            Thread.Sleep(TimeSpan.FromSeconds(5));
            steamClient.Connect();
        }

        static void OnLoggedOn(SteamUser.LoggedOnCallback callback) {
            /* 2FA | Guard check */
            bool isSteamGuard = callback.Result == EResult.AccountLogonDenied;
            bool is2FA = callback.Result == EResult.AccountLoginDeniedNeedTwoFactor;

            if (isSteamGuard | is2FA) {
                Console.WriteLine("This account is SteamGuard protected!");
                if(is2FA | isSteamGuard) { 
                    Console.WriteLine((is2FA ? "Please enter your 2 factor auth code from your authenticator app: " : String.Format("Please enter the auth code sent to the email at {0}: ", callback.EmailDomain)));
                    if (is2FA) twoFactorAuth = Console.ReadLine();
                    else authCode = Console.ReadLine();
                    return;
                }
            }


            if (callback.Result != EResult.OK) {
                Console.WriteLine("Unable to log in Steam: {0}\n", callback.Result);
                isRunning = false;
                return;
            }
            Console.WriteLine("{0} succesfully logged in!", user);
        }
        static void OnLoggedOff(SteamUser.LoggedOffCallback callback) {
            Console.WriteLine("Logged off of Steam: {0}", callback.Result);
        }

        static void OnAccountInfo(SteamUser.AccountInfoCallback callback) {
            steamFriends.SetPersonaState(EPersonaState.Offline); // TODO: change back to online
        }

        static void OnFriendsList(SteamFriends.FriendsListCallback callback) {
            // at this point, the client has received it's friends list

            int friendCount = steamFriends.GetFriendCount();

            Console.WriteLine("We have {0} friends", friendCount);

            for (int x = 0; x < friendCount; x++) {
                // steamids identify objects that exist on the steam network, such as friends, as an example
                SteamID steamIdFriend = steamFriends.GetFriendByIndex(x);

                // we'll just display the STEAM_ rendered version
                Console.WriteLine("Friend: {0}", steamIdFriend.Render());
            }

            // we can also iterate over our friendslist to accept or decline any pending invites

            foreach (var friend in callback.FriendList) {
                if (friend.Relationship == EFriendRelationship.RequestRecipient) {
                    // this user has added us, let's add him back
                    steamFriends.AddFriend(friend.SteamID);
                }
            }
        }
        static void OnFriendAdded(SteamFriends.FriendAddedCallback callback) {
            Console.WriteLine("{0} is now a friend", callback.PersonaName);
        }

        static void OnPersonaState(SteamFriends.PersonaStateCallback callback) {
            Console.WriteLine("State change: {0}", callback.Name);
        }

        static void OnChatMessage(SteamFriends.FriendMsgCallback callback) {
            if (callback.EntryType == EChatEntryType.ChatMsg) {
                if (callback.Message.Length > 1)  {
                    if(callback.Message.Remove(1) == "!") { // returns true if message starts with "!"
                        Console.WriteLine("Received message from {0}: {1}", callback.Sender, callback.Message); // log the received message on the console.
                        CommandManager.Execute(steamFriends, callback);
                        
                    }
                }
            }

        }

    }
}
