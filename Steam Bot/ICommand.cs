﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SteamKit2;

namespace Steam_Bot {

    enum ArgTypes {
        AMOUNT,
        FRIEND,
        TEXT
    }

    public interface ICommand {

        string Description { get; }
        int ArgAmount { get; } // TODO: rework this one to a list with e.g. [friend, int, string] this corresponds than to "!send johndoe 20 Hello world!" enumlist
        string UsageSyntax { get; }

        void Action(SteamFriends steamFriends, SteamFriends.FriendMsgCallback callback, string[] args);
    }

    public interface ICommandError {
        void Action(SteamFriends steamFriends, SteamFriends.FriendMsgCallback callback, string input);
    }
}
