﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SteamKit2;

namespace Steam_Bot {
    public sealed class CommandManager {

        private static readonly Lazy<CommandManager> lazy = new Lazy<CommandManager>(() => new CommandManager());
        public static CommandManager Instance { get { return lazy.Value; } }
        private CommandManager() { }

        private static Dictionary<string, ICommand> CommandList = new Dictionary<string, ICommand>();
        private static Dictionary<string, string> AliasList = new Dictionary<string, string>();

        public static string[] GetCommands() {
            List<string> KeyList = new List<string>();
            foreach (KeyValuePair<string, ICommand> s in CommandList) {
                if (s.Key != "-1") KeyList.Add(s.Key);
            }
            return KeyList.ToArray();
        }

        private static ICommand AddCommand(string name, ICommand command) {
            CommandList[name] = command;
            return command;
        }

        private static string AddAlias(string name, string alias) {
            AliasList[alias] = name;
            return name;
        }

        public static bool IsCommandOrAlias(string command) {
            return (AliasList.ContainsKey(command) || CommandList.ContainsKey(command));
        }

        public static ICommand GetCommand(string command) {
            Console.WriteLine(command);
            try {
                return CommandList[AliasList[command]];
            } catch {
                return CommandList[command];
            }
        }

        public static void Init() {
            /* Commands */
            AddCommand("help", new Commands.CommandHelp());

            AddCommand("trump", new Commands.CommandTrump());
            AddCommand("bernie", new Commands.CommandBernie());

            /* Alias */
            AddAlias("bernie", "bern");
        }

        public static void Execute(SteamFriends steamFriends, SteamFriends.FriendMsgCallback callback) {
            // TODO: Find better way of error handling
            
            try {
                string[] commandseparation = Util.Separator(callback.Message, GetCommand(Util.CommandName(callback.Message)).ArgAmount);
                if (commandseparation.Length < GetCommand(commandseparation[0].Substring(1)).ArgAmount) new Commands.CommandError().Action(steamFriends, callback, callback.Message); // get called when not enough arguments for the command are given
                else GetCommand(commandseparation[0].Substring(1)).Action(steamFriends, callback, commandseparation);
            } catch {
                // only get called when command isnt recognized
                new Commands.CommandError().Action(steamFriends, callback, callback.Message);
            }
        }

    }

}
