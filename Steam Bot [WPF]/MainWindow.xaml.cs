﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SteamKit2;
using System.Diagnostics;
using SteamWebAPI2;
using Xceed.Wpf.Toolkit;

namespace Steam_Bot__WPF_ {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        SteamBot SteamBot;
        SteamWebAPI2.Interfaces.SteamUser steamInterface = new SteamWebAPI2.Interfaces.SteamUser("DE482832770BD88039A65B2DC7B36597");

        public MainWindow() {
            InitializeComponent();

            LogIn();
            CommandManager.Init();
            LoadCommands();
        }

        private void LoadCommands() {
            CommandList.Children.Clear();
            CommandOptions.Content = "";

            foreach (ICommand command in CommandManager.GetList()) {
                Button bt = new Button() {
                    Content = command.Name,
                    Height = 24
                };

                bt.Click += delegate {
                    CommandOptions.Content = "";
                    command.DrawOptions(this, SteamBot.steamClient, SteamBot.steamFriends);
                    ButtonExecute.Click += delegate {
                        command.Action(SteamBot.steamClient, SteamBot.steamFriends);
                    };
                };

                CommandList.Children.Add(bt);
            }
        }

        private void LogIn() {
            SteamBot = new SteamBot();
            Login login = new Login();
            if (login.ShowDialog() == true) {
                SteamBot.LogIn(this, login.user.Text.Trim(), login.pass.Password.Trim());
            } else {
                Environment.Exit(0);
            }
        }

        public void Hider(bool b) {
            Dispatcher.Invoke(() => {
                if (b) Hide();
                else Show(); 
            });
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            // TODO: Logout
            Environment.Exit(0);
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            Debug.WriteLine("loading commands");
            LoadCommands();
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (TabProfile.IsSelected) {
                if (SteamBot.steamClient.IsConnected) {
                    LoadProfile();
                } else {
                    TextBlock tb = new TextBlock() {
                        Text = "You are not connected try again after you are connected."
                    };
                    ProfileInfo.Children.Add(tb);
                }
            }
        }

        private async void LoadProfile() {
            var userSummary = await steamInterface.GetPlayerSummaryAsync(SteamBot.steamClient.SteamID);

            Uri imageuri = new Uri(userSummary.Data.AvatarFullUrl, UriKind.Absolute);
            BitmapImage img = new BitmapImage(imageuri);
            Image avatar = new Image() { Source = img };

            TextBlock tbUserName = new TextBlock() { Text = userSummary.Data.Nickname };

            ProfileInfo.Children.Add(tbUserName);
            ProfileInfo.Children.Add(avatar);
        }
    }
}
