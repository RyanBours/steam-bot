﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using SteamKit2;

namespace Steam_Bot__WPF_ {
    public sealed class CommandManager {
        private static readonly Lazy<CommandManager> lazy = new Lazy<CommandManager>(() => new CommandManager());
        public static CommandManager Instance { get { return lazy.Value; } }

        private CommandManager() { }

        private static List<ICommand> CommandList = new List<ICommand>();

        public static void Init() {
            Add(new CommandSend());
            Add(new CommandTest());
        }

        public static List<ICommand> GetList() {
            return CommandList;
        }

        public static void Add(ICommand command) {
            CommandList.Add(command);
        }

    }
}
