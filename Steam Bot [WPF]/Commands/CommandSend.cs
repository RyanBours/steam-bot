﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SteamKit2;
using System.Diagnostics;
using System.Windows.Controls;
using Xceed.Wpf.Toolkit;
using System.Threading;

namespace Steam_Bot__WPF_ {
    class CommandSend : ICommand {

        SteamID receiver;

        TextBox tbMessage;
        TextBlock tbReceiver;
        IntegerUpDown amount;
        IntegerUpDown delay;

        public string Name => "Send";

        public void Action(SteamClient client, SteamFriends steamFriends) {
            if (receiver != null) {
                Debug.WriteLine("Sending: {0} to {1}[{2}] - Amount: {3}, Delay: {4}", tbMessage.Text, steamFriends.GetFriendPersonaName(receiver), receiver, amount.Value, delay.Value);
                Thread t = new Thread(() => {
                    Debug.WriteLine("new Thread");
                    string msg = "";
                    int dly=0, amn=0;
                    tbMessage.Dispatcher.Invoke(() => {
                        msg = tbMessage.Text;
                    });
                    delay.Dispatcher.Invoke(() => {
                        dly = delay.Value ?? 0;
                    });
                    amount.Dispatcher.Invoke(() => {
                        amn = amount.Value ?? 0;
                    });
                    Debug.WriteLine("{0}, {1}, {2}", msg, dly, amn);
                    for (int i = 0; i < amn; i++) {
                        steamFriends.SendChatMessage(receiver, EChatEntryType.ChatMsg, msg);
                        if (dly == 0) Thread.Sleep(5);
                        else {
                            Thread.Sleep(TimeSpan.FromSeconds(dly));
                        }
                    }
                    Debug.WriteLine("Close Thread");
                });
                t.Start();
            }
        }

        public void DrawOptions(MainWindow window, SteamClient client, SteamFriends steamFriends) {
            Grid mainGrid = new Grid();
            mainGrid.ColumnDefinitions.Add(new ColumnDefinition() {
                Width = new System.Windows.GridLength(1, System.Windows.GridUnitType.Star)
            });
            mainGrid.ColumnDefinitions.Add(new ColumnDefinition() {
                Width = new System.Windows.GridLength(2, System.Windows.GridUnitType.Star)
            });

            Grid rightGrid = new Grid();
            rightGrid.SetValue(Grid.ColumnProperty, 1);
            rightGrid.RowDefinitions.Add(new RowDefinition() {
                Height = new System.Windows.GridLength(2, System.Windows.GridUnitType.Star)
            });
            rightGrid.RowDefinitions.Add(new RowDefinition() {
                Height = new System.Windows.GridLength(6, System.Windows.GridUnitType.Star)
            });
            rightGrid.RowDefinitions.Add(new RowDefinition() {
                Height = new System.Windows.GridLength(2, System.Windows.GridUnitType.Star)
            });

            Grid numericGrid = new Grid();
            numericGrid.SetValue(Grid.RowProperty, 2);
            numericGrid.RowDefinitions.Add(new RowDefinition() {
                Height = new System.Windows.GridLength(1, System.Windows.GridUnitType.Star)
            });
            numericGrid.RowDefinitions.Add(new RowDefinition() {
                Height = new System.Windows.GridLength(1, System.Windows.GridUnitType.Star)
            });
            numericGrid.ColumnDefinitions.Add(new ColumnDefinition() {
                Width = new System.Windows.GridLength(1, System.Windows.GridUnitType.Star)
            });
            numericGrid.ColumnDefinitions.Add(new ColumnDefinition() {
                Width = new System.Windows.GridLength(1, System.Windows.GridUnitType.Star)
            });

            ScrollViewer scroll = new ScrollViewer();
            StackPanel stack = new StackPanel();
            for (int i = 0; i < steamFriends.GetFriendCount(); i++) {
                SteamID steamIdFriend = steamFriends.GetFriendByIndex(i);
                Button bt = new Button() { Content = steamFriends.GetFriendPersonaName(steamIdFriend) };
                bt.Click += delegate {
                    receiver = steamIdFriend;
                    tbReceiver.Text = "Sending to: \n" + steamFriends.GetFriendPersonaName(steamIdFriend);
                };
                stack.Children.Add(bt);
            }
            scroll.Content = stack;

            tbReceiver = new TextBlock() { Text = "Sending to: " };
            tbMessage = new TextBox();
            tbMessage.SetValue(Grid.RowProperty, 1);

            TextBlock lbAmount = new TextBlock() { Text = "Amount" };
            TextBlock lbDelay = new TextBlock() { Text = "Delay" };
            lbDelay.SetValue(Grid.ColumnProperty, 1);

            amount = new IntegerUpDown() { Value = 1, Minimum = 1 };
            amount.SetValue(Grid.RowProperty, 1);
            amount.SetValue(Grid.ColumnProperty, 0);
            delay = new IntegerUpDown() { Value = 1, Minimum = 1, Maximum = 60 };
            delay.SetValue(Grid.RowProperty, 1);
            delay.SetValue(Grid.ColumnProperty, 1);


            numericGrid.Children.Add(lbAmount);
            numericGrid.Children.Add(lbDelay);
            numericGrid.Children.Add(amount);
            numericGrid.Children.Add(delay);
            rightGrid.Children.Add(numericGrid);
            rightGrid.Children.Add(tbReceiver);
            rightGrid.Children.Add(tbMessage);
            mainGrid.Children.Add(scroll);
            mainGrid.Children.Add(rightGrid);

            window.CommandOptions.Content = mainGrid;
        }
    }
}
