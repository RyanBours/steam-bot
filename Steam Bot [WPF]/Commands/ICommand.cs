﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SteamKit2;
using System.Windows.Controls;

namespace Steam_Bot__WPF_ {
    public interface ICommand {
        string Name { get; }
        void DrawOptions(MainWindow window, SteamClient client, SteamFriends steamFriends);
        void Action(SteamClient client, SteamFriends steamFriends);
    }
}
