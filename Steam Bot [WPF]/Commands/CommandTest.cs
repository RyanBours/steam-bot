﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SteamKit2;
using System.Diagnostics;

namespace Steam_Bot__WPF_ {
    class CommandTest : ICommand {
        public string Name => "Test";

        public void Action(SteamClient client, SteamFriends steamFriends) {
            GetInfo(steamFriends);
        }

        private async void GetInfo(SteamFriends steamFriends) {
            var profileInfo = await steamFriends.RequestProfileInfo(new SteamID(76561198052430988));
            Debug.WriteLine(profileInfo);
        }

        public void DrawOptions(MainWindow window, SteamClient client, SteamFriends steamFriends) {
            
        }
    }
}
