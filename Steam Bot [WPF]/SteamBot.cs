﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Threading;
using SteamKit2;
using SteamWebAPI2;

namespace Steam_Bot__WPF_ {
    class SteamBot {
        private static readonly Lazy<SteamBot> lazy = new Lazy<SteamBot>(() => new SteamBot());

        MainWindow mainWindow;

        string user, pass;
        bool isRunning;

        public static SteamClient steamClient;
        public static CallbackManager manager;
        public static SteamUser steamUser;
        public static SteamFriends steamFriends;

        static string authCode, twoFactorAuth;

        string loginkey = null;

        public SteamBot Instance { get { return lazy.Value; } }

        public SteamBot() {
            steamClient = new SteamClient();
            manager = new CallbackManager(steamClient);

            steamUser = steamClient.GetHandler<SteamUser>();
            steamFriends = steamClient.GetHandler<SteamFriends>();


            manager.Subscribe<SteamClient.ConnectedCallback>(OnConnected);
            manager.Subscribe<SteamClient.DisconnectedCallback>(OnDisconnected);

            manager.Subscribe<SteamUser.LoggedOnCallback>(OnLoggedOn);
            manager.Subscribe<SteamUser.LoggedOffCallback>(OnLoggedOff);

            manager.Subscribe<SteamUser.UpdateMachineAuthCallback>(OnMachineAuth);
            manager.Subscribe<SteamUser.LoginKeyCallback>(OnLoginKey);
        }

        public void LogIn(MainWindow window, string user, string pass) {
            mainWindow = window;
            this.user = user;
            this.pass = pass;

            steamClient.Connect();
            isRunning = true;

            Thread t = new Thread(() => {
                while (isRunning) {
                    manager.RunWaitAllCallbacks(TimeSpan.FromSeconds(1));
                }
            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }

        static void OnMachineAuth(SteamUser.UpdateMachineAuthCallback callback) {
            Console.WriteLine("Updating sentry file...");

            int fileSize;
            byte[] sentryHash;
            using (var fs = File.Open("sentry.bin", FileMode.OpenOrCreate, FileAccess.ReadWrite)) {
                fs.Seek(callback.Offset, SeekOrigin.Begin);
                fs.Write(callback.Data, 0, callback.BytesToWrite);
                fileSize = (int)fs.Length;

                fs.Seek(0, SeekOrigin.Begin);
                using (var sha = SHA1.Create()) {
                    sentryHash = sha.ComputeHash(fs);
                }
            }

            steamUser.SendMachineAuthResponse(new SteamUser.MachineAuthDetails {
                JobID = callback.JobID,
                FileName = callback.FileName,
                BytesWritten = callback.BytesToWrite,
                FileSize = callback.Data.Length,
                Offset = callback.Offset,
                Result = EResult.OK,
                LastError = 0,
                OneTimePassword = callback.OneTimePassword,
                SentryFileHash = sentryHash,
            });

            Console.WriteLine("Done");
        }

        void OnDisconnected(SteamClient.DisconnectedCallback callback) {
            Console.WriteLine("\n{0} disconnected from steam reconnecting in 5...\n", user);
            Thread.Sleep(TimeSpan.FromSeconds(5));
            steamClient.Connect();
        }

        void OnConnected(SteamClient.ConnectedCallback callback) {
            if (callback.Result != EResult.OK) {
                Console.WriteLine("Unable to Connect to Steam: {0}", callback.Result);
                isRunning = false;
                return;
            }
            Console.WriteLine("Connected to Steam. \nLogging in {0}...\n", callback.Result);
            byte[] sentryHash = null;

            if (File.Exists("sentry.bin")) {
                byte[] sentryFile = File.ReadAllBytes("sentry.bin");
                sentryHash = CryptoHelper.SHAHash(sentryFile);
            }

            steamUser.LogOn(new SteamUser.LogOnDetails {
                Username = user,
                Password = pass,

                AuthCode = authCode,
                TwoFactorCode = twoFactorAuth,

                SentryFileHash = sentryHash,
                ShouldRememberPassword = true,
                LoginKey = loginkey
            });

            isRunning = true;

        }

        void OnLoginKey(SteamUser.LoginKeyCallback callback) {
            loginkey = callback.LoginKey;
        }

        void OnLoggedOn(SteamUser.LoggedOnCallback callback) {
            /* 2FA | Guard check */
            bool isSteamGuard = callback.Result == EResult.AccountLogonDenied;
            bool is2FA = callback.Result == EResult.AccountLoginDeniedNeedTwoFactor;

            if (isSteamGuard | is2FA) {
                Console.WriteLine("This account is SteamGuard protected!");
                if (is2FA | isSteamGuard) {
                    mainWindow.Hider(true);
                    Authenticate auth = new Authenticate();
                    if (auth.ShowDialog() == true) {
                        if (is2FA) twoFactorAuth = auth.authcode.Text;
                        else authCode = auth.authcode.Text;
                    } else {
                        Environment.Exit(0);
                    }
                    return;
                }
            }

            if (callback.Result == EResult.InvalidPassword) {
                Environment.Exit(0);
            }

            if (callback.Result != EResult.OK) {
                Console.WriteLine("Unable to log in Steam: {0}\n", callback.Result);
                isRunning = false;
                return;
            }
            Console.WriteLine("{0} succesfully logged in!", user);
            mainWindow.Hider(false);
        }
        static void OnLoggedOff(SteamUser.LoggedOffCallback callback) {
            Console.WriteLine("Logged off of Steam: {0}", callback.Result);
        }

    }
}
