﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testing {
    class Util {
        public static string[] Seperate(string str, int num = 1, char seperator = ' ') {
            string[] returned = new string[4];
            int i = 0;

            int error = 0;

            int length = str.Length;

            foreach (char c in str) {
                if (i != num) {
                    if (error > length || num > 5) {
                        returned[0] = "-1";
                        return returned;
                    } else if (c == seperator) {
                        returned[i] = str.Remove(str.IndexOf(c));
                        str = str.Remove(0, str.IndexOf(c) + 1);
                        i++;
                    }
                    error++;

                    if (error == length && i != num) {
                        returned[0] = "-1";
                        return returned;
                    } else {
                        returned[i] = str;
                    }
                }
            }
            return returned;
        }

        public static string[] Separator2(string str, int amount, char separator = ' ') {

            string[] returned = new string[amount];
            string command = str;
            try {
                for (int i = 0; i < amount--; i++) {
                    returned[i] = command.Remove(command.IndexOf(separator)); // adds all characters till the separator at index [i]
                    command = command.Replace(returned[i] + separator, ""); // removes all characters till the separator and stores it int the variable command
                }
                returned[returned.Length - 1] = command; // adds the remainder to returned at index[returned.Length - 1]
            } catch {
                // if outof bound exception set index [0] = "!-1"
                returned[0] = "!-1";
                returned[1] = str;
            }
            return returned;
        }

        public static string[] Separator(string str, int amount, char separator = ' ') {
            // no error returns only separation if possible and space removal
            // command arg[0] to lower characters
            List<string> returned = new List<string>();
            string command = str;
            try {
                for (int i = 0; i < amount--; i++) {
                    returned.Add(command.Remove(command.IndexOf(separator)));
                    command = command.Replace(command.Remove(command.IndexOf(separator)) + separator, "");
                }
                returned.Add(command);
            } catch {
                returned.Add(command);
            }

            return returned.ToArray();
        }

        public static string CommandName(string str) {
            if (str.Contains(' ')) return (str.Remove(str.IndexOf(" "))).Substring(1);
            return str.Substring(1);
        }
    }
}
