﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testing {
    class CommandTest : ICommand {
        public int Amount { get => 1; }

        public void Action(string command) {
            Console.WriteLine(command);
        }
    }
}
