﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testing {
    public interface ICommand {
        int Amount { get; }
        void Action(string command); 
    }
}
