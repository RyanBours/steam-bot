﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testing {
    public sealed class CommandManager {
        private static readonly Lazy<CommandManager> lazy =
            new Lazy<CommandManager>(() => new CommandManager());

        public static CommandManager Instance { get { return lazy.Value; } }

        private CommandManager() { }

        private static Dictionary<string, ICommand> CommandList = new Dictionary<string, ICommand>();

        private static ICommand _Add(string str, ICommand command) {
            CommandList[str] = command;
            return command;
        }
        public static void Init() {
            _Add("test", new CommandTest());
        }

        public static ICommand Execute(string command) {
            CommandList[Util.CommandName(command)].Action(command);
            return CommandList[Util.CommandName(command)];
        }

    }
}
